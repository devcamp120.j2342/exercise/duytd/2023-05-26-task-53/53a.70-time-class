import models.Time;

public class App {
    public static void main(String[] args) throws Exception {

        Time time1 = new Time(2, 25, 30);
        Time time2 = new Time(4, 40, 0);

        System.out.println(time1);
        System.out.println(time2);

        time1.nextSecond();
        time2.previousSecond();
        System.out.println("tang time1 1 giay, giam time2 mot giay");
        System.out.println(time1);
        System.out.println(time2);
    }
}
